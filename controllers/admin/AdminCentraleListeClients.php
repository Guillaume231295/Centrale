<?php

    class AdminCentraleListeClientsController extends ModuleAdminController{
        
	public function __construct()
	{
        //Information de base et permet d'afficher le contenu
        $this->bootstrap = true;
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		$this->table = 'liste_clients';
		$this->className = 'AdminCentraleListeClients';
		$this->fields_list = array(
			'id_liste_clients' => array(
                'title' => $this->l('id_liste_clients'),
                'type' => 'int'
            ),
            'name_clients' => array(
                'title' => $this->l('name_clients'),
                'type' => 'text'
            ),
            'tokken_clients' => array(
                'title' => $this->l('tokken_clients'),
                'type' => 'text'
            ),
            'URL_clients' => array(
                'title' => $this->l('URL_clients'),
                'type' => 'text'
            ),
		);
		parent::__construct();
    }

    //On affiche le formulaire
    public function renderForm(){
        $this->fields_form = array(
            'legend' => array(
            'title' => $this->l('Settings'),
            'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('name_clients'),
                    'name' => 'name_clients',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('tokken_clients'),
                    'name' => 'tokken_clients',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('URL_clients'),
                    'name' => 'URL_clients',
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save')
            ),
        );
        return parent::renderForm();         
    }
}