<?php

    class CentraleSynchroModuleFrontController extends ModuleFrontController{

    public function initContent(){

        //Central - La centrale demande au client de récupérer les données pour un update -------------(AUTOMATIQUE)---------------
        if($_GET['update']){
            //Sécurité Centrale
            $sql = 'SELECT tokken_clients FROM '._DB_PREFIX_.'liste_clients';
            $tokken_clients = Db::getInstance()->executeS($sql);
            foreach($tokken_clients as $key => $tokken){
                $tokken_client[] = $tokken['tokken_clients'];
            }
            if(in_array($_GET['tokken'], $tokken_client)){
                //Tester le  controller
                //index.php?fc=module&module=centrale&controller=synchro
                //On cherche les produits dans la classe Product avec la methode getProducts
                $Products = Product::getProducts(1, 0, NULL, 'id_product', 'ASC');
                //On fait une boucle pour chaque produit et on fait une condition pour récupérer le nombre du produit qui nous intéresse
                foreach($Products as $key => $test){
                    if($test['id_product'] == $_GET['update']){
                        $nb = $key;
                    }
                }
                 // On rajoute la clé results à $products pour le prétransformé en json
                $Json = array(
                    'results' => $Products[$nb]
                );
                //On créer le format json et on encode.
                header('Content-type:application/json');
                echo(json_encode($Json));
                die();
            }

        //Central - La centrale demande au client de récupérer les données pour un ajout de produit --------(AUTOMATIQUE)--------
        }elseif($_GET['reference']){
            //Sécurité Centrale
            $sql = 'SELECT tokken_clients FROM '._DB_PREFIX_.'liste_clients';
            $tokken_clients = Db::getInstance()->executeS($sql);
            foreach($tokken_clients as $key => $tokken){
                $tokken_client[] = $tokken['tokken_clients'];
            }
            if(in_array($_GET['tokken'], $tokken_client)){
                //Recherche de l'id_products
                $sql = 'SELECT id_product 
                        FROM '._DB_PREFIX_.'product
                        WHERE reference = "'.$_GET['reference'].'"';
                $id_products = Db::getInstance()->executeS($sql);
                //index.php?fc=module&module=centrale&controller=synchro
                $Products = Product::getProducts(1, 0, NULL, 'id_product', 'ASC');
                //On fait une boucle pour chaque produit et on fait une condition pour récupérer le nombre du produit qui nous intéresse
                foreach($Products as $key => $test){
                    if($test['id_product'] == $id_products[0]['id_product']){
                        $nb = $key;
                    }
                }
                // On rajoute la clé results à $products pour le prétransformé en json
                $Json = array(
                    'results' => $Products[$nb]
                );
                //On créer le format json et on encode.
                header('Content-type:application/json');
                echo(json_encode($Json));
                die();
            }

        // CLIENT - Le client récupère directement toutes les données et les traites -----(MANUEL)------
        }else{
            //Sécurité Centrale
            $sql = 'SELECT tokken_clients FROM '._DB_PREFIX_.'liste_clients';
            $tokken_clients = Db::getInstance()->executeS($sql);
            foreach($tokken_clients as $key => $tokken){
                $tokken_client[] = $tokken['tokken_clients'];
            }
            if(in_array($_GET['tokken'], $tokken_client)){
                //index.php?fc=module&module=centrale&controller=synchro
                $Products = Product::getProducts(1, 0, NULL, 'id_product', 'ASC');
                // On rajoute la clé results à $products pour le prétransformé en json
                $Json = array(
                    'results' => $Products
                );
                //On créer le format json et on encode.
                header('Content-type:application/json');
                echo(json_encode($Json));
                die();
            }
        }
    }
}