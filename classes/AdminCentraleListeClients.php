<?php 

class AdminCentraleListeClients extends ObjectModel{
    /* 
    * @param id_disii_name type INT
    */

    public $id_liste_clients;
    public $name_clients;
    public $tokken_clients;
    public $URL_clients;

    //Permet de faire des modifications dans la base de donnée
    public static $definition = array(
        'table' => 'liste_clients',
        'primary' => 'id_liste_clients',
        'multilang' => false,
        'fields' => array(
            'name_clients' => array('type' => self::TYPE_STRING,
                            'validate' => 'isString',
                            'size' => 128),

            'tokken_clients' => array('type' => self::TYPE_STRING,
                            'validate' => 'isString',
                            'size' => 255),
            'URL_clients' => array('type' => self::TYPE_STRING,
                            'validate' => 'isString',
                            'size' => 255),
        )
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null){
        parent::__construct($id, $id_lang, $id_shop);
    }  
}



