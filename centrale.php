<?php
    require_once(dirname(__FILE__).'/classes/AdminCentraleListeClients.php');

    class Centrale extends Module {

        
    //On initialise les données du module
    public function __construct(){
        $this->name = 'centrale'; 
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Guillaume Module Centrale DV';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6.1.18');
        $this->bootstrap = true;
        $this->displayName = $this->l('Central');
        $this->description = $this->l('The module of the central');
        $this->confirmUninstall = $this->l('Really ? Are you sure ? ');
        parent::__construct();
    }

    // On créer une fonction pour installer le module
    public function install(){  
        if(!parent::install()
            || !$this->installSql()
            || !$this->_installTab(0, 'AdminCentrale', $this->l('Module centrale'))
            || !$this->_installTab('AdminCentrale', 'AdminCentraleListeClients', $this->l('Liste clients'))
            // Hook pour l'envoi de donnée après l'update des produits
            || !$this->registerHook('actionObjectProductUpdateAfter')
            // Hook pour l'envoi de donnée après l'ajout de produits
            || !$this->registerHook('actionObjectProductAddAfter')
        ){
            return false;
        }
        return true;
    }

    // On créer une fonction pour la desinstallation du module
    public function uninstall(){
        if(!parent::uninstall()
            || !$this->_unistallTab('AdminCentrale')
            || !$this->_unistallTab('AdminCentraleListeClients')
        ){
            return false;
        }
        return true;
    }

    // Ajout menu back office
    private function _installTab($parent, $class_name, $name){
        $tab = new tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->name= array();
        foreach(Language::getLanguages(true) as $lang){
            $tab->name[$lang['id_lang']] = $name;
        }
        return $tab->save();
    }

    //Suppression menu back office
    private function _unistallTab($class_name){
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        $tab = new Tab($id_tab);
        return $tab->delete();
    }

    // Inclu fichier pour faire du SQL
    public function installSql(){
        include(dirname(__FILE__).'/sql/install.php');
        $result = true;
        foreach($sql_requests as $request){
            if(!empty($request)){
                $result &= Db::getInstance()->execute(trim($request));
            }
        }
        return $result;
    }

    // Hook d'action pour envoi après update produit
    public function hookActionObjectProductUpdateAfter($params)
    {
        //Recherche du tokken Client et l'url de chaque clients
        $sql = 'SELECT tokken_clients, URL_clients 
                FROM '._DB_PREFIX_.'liste_clients';
        $recherches = Db::getInstance()->executeS($sql);

        //ON BOUCLE SUR CHAQUE CLIENT 
        foreach($recherches as $recherche){
            // Objet Produit avec les valeurs modifiées		
            $newProduct = $params['object'];
            //CURL vers contrôleur front du module client
            define('PRESTASHOP_URLE',''.$recherche['URL_clients'].'index.php?fc=module&module=client&controller=synchro&tokken='.$recherche['tokken_clients'].'&update='.$newProduct->id); 
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, PRESTASHOP_URLE); 
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
            $json = curl_exec($ch);
        }
    }

    // Hook d'action pour envoi après ajout produits
    public function hookActionObjectProductAddAfter($params)
    {
        //Recherche du tokken Client et l'url de chaque clients
        $sql = 'SELECT tokken_clients, URL_clients 
                FROM '._DB_PREFIX_.'liste_clients';
        $recherches = Db::getInstance()->executeS($sql);

        //ON BOUCLE SUR CHAQUE CLIENT
        foreach($recherches as $recherche){
            // Objet Produit avec les valeurs modifiées
            $newProduct = $params['object'];
            //CURL vers contrôleur front du module client
            define('PRESTASHOP_URL',''.$recherche['URL_clients'].'index.php?fc=module&module=client&controller=synchro&tokken='.$recherche['tokken_clients'].'&reference='.$newProduct->reference);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, PRESTASHOP_URL); 
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
            $json = curl_exec($ch);
        }
    }  
}